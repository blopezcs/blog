<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=> 'blog.home', 'uses'=>'PostsController@index']);

Route::get('/posts/create',['as'=>'post.create', 'uses'=>'PostsController@create']); 

Route::post('/posts',['as'=>'post.store','uses'=>'PostsController@store']);

Route::get('/posts/{post}', ['as' => 'post.show', 'uses' => 'PostsController@show']);

Route::get('/searchTitles', ['as' => 'search.titles', 'uses' => 'PostsController@searchTitles']);

Route::get('/posts/searchTitles/results', ['as' => 'search.titlesResults', 'uses' => 'PostsController@searchResultsTitles']);

Route::get('/searchTags', ['as' => 'search.tags', 'uses' => 'PostsController@searchTags']);

Route::get('/posts/searchTags/results', ['as' => 'search.tagsResults', 'uses' => 'PostsController@searchResultsTags']);

Route::get('/searchProfiles', ['as' => 'search.profiles', 'uses' => 'PostsController@searchProfiles']);

Route::get('/searchProfiles/results', ['as' => 'search.profiles', 'uses' => 'PostsController@searchResultsProfiles']);

Route::get('/posts/{post}/edit/', ['as' => 'post.edit', 'uses' => 'PostsController@edit']);

Route::delete('/posts/{post}/delete/', ['as' => 'post.delete', 'uses' => 'PostsController@delete']);

Route::put('/posts/{post}/update/', ['as' => 'post.update', 'uses' => 'PostsController@update']);

Auth::routes();

Route::post('/posts/{post}/comments',['as'=>'post.addComment','uses'=>'PostsController@addComment']);

Route::get('/posts/{post}/{comment}/editComment/', ['as' => 'comment.edit', 'uses'=> 'PostsController@editComment']);

Route::delete('/posts/{post}/{comment}/deleteComment/', ['as' => 'comment.delete', 'uses' => 'PostsController@deleteComment']);

Route::patch('/posts/{post}/{comment}/updateComment/', ['as'=> 'comment.update', 'uses' => 'PostsController@updateComment']);

Route::get('/profile', ['as'=>'post.myProfile','uses'=>'PostsController@myProfile']);

Route::post('/profile', ['as'=>'profile.uploadPicture','uses'=>'PostsController@profilePicture']);

Route::get('/profile/{user?}', ['as'=>'post.profile','uses'=>'PostsController@profile']);

Route::get('/AboutMe', ['as'=>'aboutme','uses'=>'MoviesController@aboutMe']);



Route::get('/movieSearch', ['as'=>'movieSearch','uses'=>'MoviesController@movieSearch']);

Route::post('/movieSearch/results', ['as'=>'movieSearchResults','uses'=>'MoviesController@movieSearchResults']);

Route::post('/profile/addMovie', ['as'=>'addMovie', 'uses'=>'MoviesController@saveMovie']);

Route::post('/profile/movies/delete/{movie}', ['as'=>'deleteMovie', 'uses'=>'MoviesController@deleteMovie']);

Route::get('/showMovies', ['as'=>'myMovies', 'uses'=>'MoviesController@myMovies']);

Route::get('/profile/{user?}/showMovies', ['as'=>'profile.movies','uses'=>'MoviesController@showMovies']);

