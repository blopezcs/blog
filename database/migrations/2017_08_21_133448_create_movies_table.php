<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('movie_id');
            $table->integer('user_id');
            $table->text('title');
            $table->text('slug');
            $table->text('overview');
            $table->text('release_date');
            $table->integer('runtime');
            $table->text('tagline');
            $table->integer('vote_average');
            $table->string('poster');
            $table->timestamps();
        });
    }

    //Genres, title, overview, release_date, runtime, tagline, vote_average, id

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
