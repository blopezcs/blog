<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\CreateCommentRequest;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Image;
use File;
use GuzzleHttp\Client;

use GuzzleHttp\Exception\RequestException;


class PostsController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth')->except(['index', 'show', 'searchTitles', 'searchTags', 'searchProfiles',
            'apiTest', 'apiTestResults']);
    }
    public function index(){

        $posts = Post::latest()->paginate(5);

        return view('posts.index',compact('posts'));
    }

    public function show(Post $post){

        return view('posts.show', compact('post'));
    }

    public function searchTitles(){
        return view('posts.searchTitles');
    }

    public function searchResultsTitles(Request $request){
        $title = request('title');
        $posts = Post::where('title','LIKE','%'.$title.'%')->paginate(5);

        return view('posts.results',compact('posts'));
    }


    public function searchTags(){
        return view('posts.searchTags');
    }

    public function searchResultsTags(Request $request){
        $tags = request('tags');
        $posts = Post::withAllTags($tags)->paginate(5);

        return view('posts.results',compact('posts'));
    }

    public function searchProfiles(){
        return view('posts.searchProfiles');
    }

    public function searchResultsProfiles(Request $request){
        $username = request('name');
        $profiles = User::where('name','LIKE','%'.$username.'%')->paginate(5);

        return view('posts.profileResults',compact('profiles'));
    }

    public function create(){
        return view('posts.create');
    }

    public function store(CreatePostRequest $request){

        $post = Post::create([
                'title' => request('title'),
                'body' => request('body'),
                'user_id' => auth()->id()
    		]);

        $tags=$request->tags;
        $post->tag($tags);
        
    	return redirect('/profile');
    }

    
    public function addComment(CreateCommentRequest $request, Post $post){

        Comment::create([
            'body'=>request('body'),
            'user_id'=> auth()->id(),
            'commentable_id'=>$post->id,
            'commentable_type'=>'App\Models\Post'
            ]);

        return redirect('/');
    }

    public function edit(Post $post){

        return view('posts.edit', compact('post'));
    }

    public function editComment(Post $post, Comment $comment){

        return view('posts.editComment', compact('comment', 'post'));
    }

    /**
     * Updates a post.
     *
     * @param $request
     * @param $post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreatePostRequest $request, Post $post){
        // TODO -- make these comments into what we refer to as DocBlocks. The precede a function or class and
        // describe the function in detail. I created an example for this one. Reserve standard comments like the one
        // below to do more detailed descriptions of particular parts of your code.

        $input=$request->all();

        $post->fill($input)->save();

        $post->retag($request->taggles);

        return redirect('/profile');
    }

    public function updateComment(CreateCommentRequest $request, Post $post, Comment $comment){ 

        $input=$request->all();

        $comment->fill($input)->save();

        return redirect('/'); //TODO -- find better redirect location?
    }

    public function delete(Post $post){

        $post->delete();
        return redirect('/profile');
    }

    public function deleteComment(Post $post, Comment $comment){

        $comment->delete();
        return redirect('/'); //TODO -- find better redirect location?
    }

    public function myProfile(){

        $user = Auth::user();
        $posts=$user->posts()->orderBy('created_at', 'desc')->paginate(5);
        return view('posts.profile',compact('posts', 'user'));
    }

    public function profile(User $user){
        if($user->id === Auth::user()->id){
            return redirect('/profile');
        }
        
        $posts=$user->posts()->orderBy('created_at', 'desc')->paginate(5);

        return view('posts.profile',compact('posts','user'));
    }

    public function profilePicture(Request $request){
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar -> getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename ));

            $user = Auth::user();
            if($user->avatar !== 'default.jpg'){
                $file = 'uploads/avatars/'.$user->avatar;
                if(File::exists($file)){
                    unlink($file);
                }
            }

            $user->avatar = $filename;
            $user->save();
        }
        return redirect('/profile');
    }

}
