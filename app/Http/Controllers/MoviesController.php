<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Movie;
use File;


use GuzzleHttp\Client;

use GuzzleHttp\Exception\RequestException;


class MoviesController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['aboutMe', 'movieSearch', 'movieSearchResults']);
    }

    public function movieSearch(){
        return view ('movies.movieSearch');

    }

    public function movieSearchResults(Request $request){
        $client = new Client;

        $id = $request->movie_id;

        if(Auth::check()){
            $user = Auth::user();

            $movieCheck = $user->movies()->where('movie_id',$id)->get();

            if(!$movieCheck->isEmpty()){
                $error = "You have already added this movie to your profile.";
                return view('movies.movieSearch', compact('error'));
            }
        }

        try{
            $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        }catch(RequestException $e){
            $error = "Movie not found. Please try a different ID.";
            return view('movies.movieSearch', compact('error'));
        }
        



        $body = $response->getBody();
        $stringBody = (string) $body;
        $data = json_decode($stringBody, 1);

        return view('movies.movieSearchResults', compact('data', 'stringBody'));

    }

    public function saveMovie(Request $request){
    	$stringBody = $request -> movieData;
    	$data = json_decode($stringBody, 1);

        $client = new Client;

        $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$data['poster_path']);


        $img_body = $img_response->getBody();
        $img_stringBody = (string) $img_body;

        $filename= $data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        Movie::create([
            'movie_id'=>$data['id'],
            'user_id'=>auth()->id(),
            'title'=>$data['title'],
            'overview'=>$data['overview'],
            'release_date'=>$data['release_date'],
            'runtime'=>$data['runtime'],
            'tagline'=>$data['tagline'],
            'vote_average'=>$data['vote_average'],
            'poster'=>$filename
            ]);

        return redirect('/showMovies');
    }

    public function myMovies(){

        $user = Auth::user();
        $movies=$user->movies()->orderBy('created_at', 'desc')->paginate(5);
        return view('movies.showMovies',compact('movies','user'));
    }

    public function showMovies(User $user){
        if($user->id === Auth::user()->id){
            return redirect('/showMovies');
        }

        $movies=$user->movies()->orderBy('created_at', 'desc')->paginate(5);        
        return view('movies.showMovies',compact('movies','user'));
    }

    public function deleteMovie(Movie $movie){
        $movie->delete();
        return redirect('/showMovies');
    }

    public function aboutMe(){
        $client = new Client;

        //Creed
        $id = '312221';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $creed_data = json_decode($stringBody, 1);

        
        $filename= $creed_data['id'].'.png';

       if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$creed_data['poster_path']);

            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;

            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //The Dark Knight
        $id = '155';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $dark_knight_data = json_decode($stringBody, 1);

        

        $filename= $dark_knight_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){

            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$dark_knight_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //The Nice Guys
        $id = '290250';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $nice_guys_data = json_decode($stringBody, 1);

        

        $filename= $nice_guys_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){

            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$nice_guys_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;


            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //The Big Lebowski
        $id = '115';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $lebowski_data = json_decode($stringBody, 1);

        

        $filename= $lebowski_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$lebowski_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;

            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Spider-Man: Homecoming
        $id = '315635';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $spiderman_data = json_decode($stringBody, 1);

        $filename= $spiderman_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$spiderman_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;

            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Guardians of the Galaxy
        $id = '118340';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $guardians_data = json_decode($stringBody, 1);

        $filename= $guardians_data['id'].'.png';

        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$guardians_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Superbad
        $id = '8363';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $superbad_data = json_decode($stringBody, 1);

        

        $filename= $superbad_data['id'].'.png';

        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$superbad_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;

            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Pirates of the Caribbean: The Curse of the Black Pearl
        $id = '22';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $pirates_data = json_decode($stringBody, 1);

        

        $filename= $pirates_data['id'].'.png';

        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$pirates_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Killer Klownz from Outer Space
        $id = '16296';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $klownz_data = json_decode($stringBody, 1);

        
        $filename= $klownz_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$klownz_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //The Expendables
        $id = '27578';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $expendables_data = json_decode($stringBody, 1);

       

        $filename= $expendables_data['id'].'.png';
 
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$expendables_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //The Prestige
        $id = '1124';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $prestige_data = json_decode($stringBody, 1);

        $filename= $prestige_data['id'].'.png';
        
        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$prestige_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }

        //Kung Fu Hustle
        $id = '9470';
        $response = $client->request('get', 'https://api.themoviedb.org/3/movie/'.$id.'?api_key=' . env('TMDB_KEY'));
        $body = $response->getBody();
        $stringBody = (string) $body;
        $kung_fu_data = json_decode($stringBody, 1);
        
        $filename= $kung_fu_data['id'].'.png';

        if(!File::exists(public_path('/uploads/moviePosters/' . $filename))){
            $img_response = $client->request('get', 'https://image.tmdb.org/t/p/w300/'.$kung_fu_data['poster_path']);
            $img_body = $img_response->getBody();
            $img_stringBody = (string) $img_body;
            $image = \Image::make($img_stringBody)->encode('png');
            $image->save(storage_path('/public/uploads/moviePosters/' . $filename ));
        }



        $movieData = array($creed_data, $dark_knight_data, $nice_guys_data, $lebowski_data, $spiderman_data, $guardians_data, $superbad_data, $pirates_data, $klownz_data, $expendables_data, $prestige_data, $kung_fu_data);




        return view('aboutme', compact('movieData'));
    }


}