<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Comment extends Model
{
	use Sluggable;

	protected $fillable = ['body', 'user_id', 'commentable_id', 'commentable_type', 'slug'];

    public function commentable(){
    	return $this->morphTo();
    }
    public function user(){
    	return $this->belongsTo('App\Models\User');
    }

    public function sluggable(){
		return [
            'slug' => [
                'source' => 'body'
            ]
        ];
	}

	public function getRouteKeyName(){
		return 'slug';
	}
}
