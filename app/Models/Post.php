<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentTaggable\Taggable;

use Carbon\Carbon;

class Post extends Model
{
	use Sluggable;
	use Taggable;

    protected $fillable = ['title', 'body', 'user_id', 'slug'];

	public function user(){

		return $this->belongsTo('App\Models\User');
	}

	public function comments(){
		
		return $this->morphMany('App\Models\Comment', 'commentable');
	}

	public function sluggable(){
		return [
            'slug' => [
                'source' => 'title'
            ]
        ];
	}

	public function getRouteKeyName(){
		return 'slug';
	}

	
}
