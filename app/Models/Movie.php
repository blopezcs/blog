<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Movie extends Model
{

	use Sluggable;

	protected $fillable = ['movie_id', 'user_id', 'title', 'overview', 'release_date', 'runtime', 'tagline', 'vote_average', 'poster', 'slug'];


	public function user(){
    	return $this->belongsTo('App\Models\User');
    }

    public function sluggable(){
		return [
            'slug' => [
                'source' => 'title'
            ]
        ];
	}

	public function getRouteKeyName(){
		return 'slug';
	}
    
}
