
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Brian Lopez's Blog</title>

    @stack('before-css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }
    </style>
    @stack('after-css')

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Faustina" rel="stylesheet">


  </head>

  <body style="background:
linear-gradient(45deg, #92baac 45px, transparent 45px)64px 64px,
linear-gradient(45deg, #92baac 45px, transparent 45px,transparent 91px, #e1ebbd 91px, #e1ebbd 135px, transparent 135px),
linear-gradient(-45deg, #92baac 23px, transparent 23px, transparent 68px,#92baac 68px,#92baac 113px,transparent 113px,transparent 158px,#92baac 158px);
background-color:#e1ebbd;
background-size: 128px 128px;">

    @include('partials.nav')

    <div class="container" style="background-color: white; border: 5px solid; border-radius: 30px; margin-top: 20px; padding-top: 10px;">
      @yield('content')

      <hr>

      @include('partials.footer')
    </div>


    @stack('before-js')
    <script src="{{ asset('js/app.js') }}"></script>  
    <script src="/bower_components/tinymce/tinymce.js"></script>
    @stack('after-js')
  </body>
</html>
