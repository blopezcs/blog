@extends ('base')

@section('content')

<div class = "container">
	<div class = "row">
			<img src="/uploads/avatars/{{$user->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px">
			<h2>{{$user->name}}'s Profile</h2>
			@if(auth()->id()===$user->id)
				<form enctype="multipart/form-data" action="/profile" method="POST">
					{{ csrf_field() }}
					<label>Update Profile Picture</label>
					<input type="file" name="avatar">
					<input type="submit" class="btn btn-sm btn-primary">
				</form>
			@endif
	</div>
</div>
<h1><a href="{{ route('profile.movies', ['slug' => $user->slug]) }}"> My Favorite Movies</a></h1>
<h1>My Posts</h1>
<div class = "container">
	<div class = "row">
		<div class = "col-md-10 col-md-offset-1">
@include('partials.showPosts', ['$posts'])
		</div>
	</div>
</div>

@endsection