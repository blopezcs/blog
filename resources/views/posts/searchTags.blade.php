@extends('base')

@section('content')
    <h1>Search Posts by Tags</h1>

    <hr>

{{-- TODO -- Format this search box with taggle for clarity in what user is searching for--}}

<form method="GET" action="/posts/searchTags/results">
	{{ csrf_field() }}
  <div class="form-group">
    <label for="title">Enter your search here </label>
    <textarea id-"tags" name="tags" class="form-control"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Search</button>
  <hr>
</form> 

@include ('partials.errors')

@stop  