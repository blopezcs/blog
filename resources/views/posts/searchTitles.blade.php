@extends('base')

@section('content')
    <h1>Search Posts by Title</h1>

    <hr>

<form method="GET" action="/posts/searchTitles/results">
	{{ csrf_field() }}
  <div class="form-group">
    <label for="title">Enter your search here </label>
    <textarea id-"title" name="title" class="form-control"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Search</button>
  <hr>
</form> 

@include ('partials.errors')

@stop  