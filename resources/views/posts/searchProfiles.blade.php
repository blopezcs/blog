@extends('base')

@section('content')
    <h1>Search Profiles</h1>

    <hr>

<form method="GET" action="/searchProfiles/results">
	{{ csrf_field() }}
  <div class="form-group">
    <label for="name">Enter the Profile name you'd like to search for here:</label>
    <textarea id-"name" name="name" class="form-control"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Search Profiles</button>
  <hr>
</form> 

@include ('partials.errors')

@stop