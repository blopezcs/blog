@extends ('base')

@section('content')

@php
    $posts= array($post);
@endphp
  
@include('partials.showPosts', ['$posts'])

@endsection