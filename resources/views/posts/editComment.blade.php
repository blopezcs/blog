@extends('base')

@section('content')

<h1>Editing Comment</h1>
<p class="lead">Edit and save this comment below, or <a href="/profile">go back to your profile.</a></p>
<hr>

@include ('partials.errors')


{{ Form::model($comment, ['method' => 'PATCH','route' => ['comment.update', $post->slug, $comment->slug]]) }}

<div class="blog-post">
    <h2 class = "blog-post-title">       
        {{$post->title}}

    </h2>

    <p class="blog-post-meta">
        {{$post->user->name}}
        {{$post->created_at->toFormattedDateString()}}
    </p>

    {!!$post->body!!}
    
</div>

<div class="form-group">
    {!! Form::label('body', 'Body:', ['class' => 'control-label']) !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}

@push('after-js')

<script>
  var editor_config = {
    path_absolute : "",
    selector: '#body',
    height: 500,
    plugins: ['advlist charmap textcolor colorpicker emoticons help lists link wordcount image media'],
    toolbar: ['charmap forecolor backcolor emoticons help numlist bullist link image media fontselect fontsizeselect'],
    menubar:['insert, tools'],
    relative_urls: false,
    height: 129,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + "{{ url(config('lfm.prefix')) }}" + '?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };
  tinymce.init(editor_config);
</script>
@endpush

@stop  