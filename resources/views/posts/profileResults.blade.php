@extends ('base')

@section('content')
    @if(count($profiles)<1)
        <h2> No Results Found. Maybe try different keywords. </h2>
        <a class="nav-link ml-auto" href="/searchTitles">Search Posts by Title</a> <br>
        <a class="nav-link ml-auto" href="/searchTags">Search Posts by Tags</a> <br>
        <a class="nav-link ml-auto" href="/searchProfiles">Search Profiles by Username</a> <br>

    @endif

@foreach ($profiles as $user)

<p style="font-size:20pt;"><a href="{{ route('post.profile', ['slug' => $user->slug]) }}">{{$user->name}}</a><p>

@endforeach

@endsection