@extends('base')

@push('after-css')
  <style>
  .taggle_input{
    width: 90% !important;
  }
  .taggle_sizer{
    display: none;
  }
  </style>
@endpush

@section('content')
<h1>Post Creation Form</h1>

<hr>

<form method="POST" action="/posts" id="create_post_form">
  {{ csrf_field() }}
  <div class = "form-group">
    <label for = "title"> Title </label>
    <input type = "text" class = "form-control" id = "title" name = "title">
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea id = "body" name = "body" class = "form-control"></textarea>
  </div>
  <div class="form-group">
    <label for="tags">Tags (optional): (To enter a tag, type the tag value, then press enter)</label>
    <div id="tags"></div>
    <div id="tag_values"></div>
  </div>
  <button type = "submit" class = "btn btn-primary">Post</button>
  <hr>
</form> 


@include ('partials.errors')

@push('after-js')
<script>
  var editor_config = {
    path_absolute : "",
    selector: '#body',
    height: 500,
    plugins: ['advlist charmap textcolor colorpicker emoticons help lists link wordcount image media'],
    toolbar: ['charmap forecolor backcolor emoticons help numlist bullist link image media fontselect fontsizeselect'],
    menubar: ['insert, tools'],
    relative_urls: false,
    height: 129,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + "{{ url(config('lfm.prefix')) }}" + '?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };
  tinymce.init(editor_config);
</script>
<script>
    var tags = new Taggle('tags', {
      onTagAdd: function(e, tag){
        $('#tag_values').append('<input type="hidden" name="tags[]" value="' + tag + '">');
      },
      onTagRemove: function(e, tag){
        $('#tag_values').find('input[value="' + tag +'"]').remove();
      }
    });
</script>
@endpush

@stop  