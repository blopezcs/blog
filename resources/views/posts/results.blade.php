@extends ('base')

@section('content')
    @if(count($posts)<1)
        <h2> No Results Found. Maybe try different keywords. </h2>
        <a class="nav-link ml-auto" href="/searchTitles">Search Posts by Title</a> <br>
        <a class="nav-link ml-auto" href="/searchTags">Search Posts by Tags</a> <br>
        <a class="nav-link ml-auto" href="/searchProfiles">Search Profiles by Username</a> <br>

    @endif

@include('partials.showPosts', ['$posts'])

@endsection