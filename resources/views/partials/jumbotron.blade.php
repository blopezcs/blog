<div class="jumbotron">	
	<div class="container">
		<h1>Welcome!</h1>
		<img src="/uploads/avatars/Blue_Devil.png" style="float:left; padding-right:10px;width:200px; height:160px;">
		<p>My name is Brian Lopez. I'm a computer science student at Duke University and am part of the class of 2018. This is a Laravel 5 blog I built to learn basic web development. Feel free to make an account and test out features. To contact me, visit the About Me page linked below.</p>
		<p><a class="btn btn-primary btn-lg" href="{{ route('aboutme') }}" role="button">About Me &raquo;</a></p>
	</div>
</div>
