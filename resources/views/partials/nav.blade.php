    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" style="font-size:175%; padding-right: 40px;" href="/">Brian's First Blog</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
             @if(Auth::check())
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="width:32px; height:32px; position:absolute; top:10px; right:100px; border-radius:50%;">
                {{Auth::user()->name}} 
                <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/profile">My Profile</a></li>
                <li><a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a></li>
              </ul>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
            <li><a href="/posts/create">Create Post</a></li>
            @else
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
            @endif
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Search <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/searchTitles">By Title</a></li>
                <li><a href="/searchTags">By Tags</a></li>
                <li><a href="/searchProfiles">Search Profiles</a></li> 
              </ul>
            <li><a href="{{ route('movieSearch') }}">TMDB Movie Search</a></li>
            <li><a href="{{ route('aboutme') }}">About Me</a></li>
            </li>
          </ul>
        </div>
      </div>
    </nav>