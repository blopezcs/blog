@foreach ($posts as $post)
<div class="blog-post">
    <h2 class = "blog-post-title" style="font-family: 'Faustina', serif;">
        @if(Route::currentRouteName() != 'post.show')
        <a href="{{ route('post.show', ['slug' => $post->slug]) }}" style="color:#636B6F">
            {{$post->title}} 
        </a>
        @else
        {{$post->title}}
        @endif

    </h2>

    <p class="blog-post-meta">
       <a href="{{ route('post.profile', ['slug' => $post->user->slug]) }}" style="color:#636B6F"> {{$post->user->name}} </a>
        {{$post->created_at->toFormattedDateString()}}
    </p>

    @if(auth()->id() == $post->user_id)
    {{ Form::open(['method' => 'DELETE', 'route' => ['post.delete', $post->slug]]) }}
    <div class="btn-group" role="group">
        <a href="{{ route('post.edit', ['post' => $post->slug]) }}" class="btn btn-primary btn-xs">Edit</a>
        {{ Form::hidden('id', $post->id) }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
    </div>
    {{ Form::close() }}
    @endif
    <article style="padding:20px; font-family: 'Lato', sans-serif;">
    {!!$post->body!!}
    </article>

    <h4>Tags:</h4>
    @if(count($post->tags)<1)
      None
    @else
      @foreach ($post->tags as $tag)
        @if($loop->last)
          {!!$tag->normalized!!}
        @else
          {!!$tag->normalized!!},
        @endif 
      @endforeach
    @endif
</div>
<h4>Comments:</h4>
<div class="comments">
    <ul class="list-group">
        @foreach ($post->comments as $comment)
        <li class="list-group-item">
            <p class="blog-comment-meta">
                {{$comment->user->name}}
                {{$comment->created_at->toFormattedDateString()}}
            </p>
            <article>
                {!!$comment->body!!}
            </article>
                @if(Auth::check() && Auth::user()->name == $comment->user->name)
                {{ Form::open(['method' => 'DELETE', 'route' => ['comment.delete', $post->slug, $comment->slug]]) }}
                <div class="btn-group" role="group">
                    <a href="{{ route('comment.edit', ['comment' => $comment->slug, 'post' => $post->slug]) }}" class="btn btn-primary btn-xs">Edit</a>
                    {{ Form::hidden('id', $comment->id) }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                </div>
                {{ Form::close() }}
                @endif
        </li>
        @endforeach
    </ul>
</div>

<hr>
@if(Auth::check())
    <div class="card">
        <form method="POST" action="/posts/{{$post->slug}}/comments/">
            {{ csrf_field() }}
            <div class="form-group">
                <textarea name="body" placeholder="Add comment here." class="form-control"> <p></p></textarea>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary"> Add Comment</button>
        </div>
    </form>
@endif

@include ('partials.errors')

@endforeach

@if($posts instanceof Illuminate\Pagination\LengthAwarePaginator)
  {{$posts->links()}}
@endif

@push('after-js')

<script>
var editor_config = {
  path_absolute : "",
  selector: '.form-control',
  height: 100,
  plugins: ['advlist charmap textcolor colorpicker emoticons help lists link wordcount image media placeholder'],
  toolbar: ['charmap forecolor backcolor emoticons help numlist bullist link image media fontselect fontsizeselect'],
  menubar:['insert, tools'],
  relative_urls: false,
  height: 129,
  file_browser_callback : function(field_name, url, type, win) {
    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

    var cmsURL = editor_config.path_absolute + "{{ url(config('lfm.prefix')) }}" + '?field_name=' + field_name;
    if (type == 'image') {
      cmsURL = cmsURL + "&type=Images";
    } else {
      cmsURL = cmsURL + "&type=Files";
    }

    tinyMCE.activeEditor.windowManager.open({
      file : cmsURL,
      title : 'Filemanager',
      width : x * 0.8,
      height : y * 0.8,
      resizable : "yes",
      close_previous : "no"
    });
  }
};

tinymce.init(editor_config);
</script>
@endpush