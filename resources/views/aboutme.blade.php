@extends('base')

@section('content')
    <h1>About Me:</h1>

    <h2>My Resume:</h2>

    <div align="center">
    <embed src="/uploads/resume.pdf" width="100%" height="700" type='application/pdf'>
    </div>


    <hr>

    <h2>My Favorite Movies:</h2>

    @foreach($movieData as $i => $data)
    	@if(($i % 3) === 0)
    		<div class="row">
    	@endif
	    	<div class="col-md-4 text-center">

				<img src= "/uploads/moviePosters/{{$data['id']}}.png"><br />

	    		<b style="font-size: 12pt;"> {{$data['title']}} </b> <br />

	    		Tagline: {{$data['tagline']}} <br />

				Released: {{$data['release_date']}} <br />

				Runtime: {{$data['runtime']}} minutes <br/ >

				Average TMDB score: {{$data['vote_average']}} <br />

				<br />

	    	</div>
    	@if(($i % 3) === 2)
    		</div>
    	@endif
    @endforeach

@stop  