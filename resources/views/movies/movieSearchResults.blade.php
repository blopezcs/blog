@extends('base')

@section('content')

<br>

<img src= "https://image.tmdb.org/t/p/w300/{{$data['poster_path']}}">

<br>
Title: {{$data['title']}}
<br>
Tagline: {{$data['tagline']}}
<br>
Summary: {{$data['overview']}}
<br>
Released: {{$data['release_date']}}
<br>
Runtime: {{$data['runtime']}} minutes
<br>


@if(Auth::check())

<h3>Add this to your Movie List?</h3>

<form method="POST" action="/profile/addMovie" id="movie_save">
	{{ csrf_field() }}

	<input type="hidden" id = "movieData" name="movieData" value="{{$stringBody}}">

	<button type = "submit" class = "btn btn-primary">Yes</button>
</form>

@endif

@stop