@extends ('base')

@section('content')

	@if(auth()->id() == $user->id)

		<h1>My Movies</h1>

	@else

		<h1> {{$user->name}}'s Movies </h1>

	@endif

	@if(count($movies)<1)

		<h2> No Movies Found</h2>

	@endif

	@foreach ($movies as $movie)

		<br>
		<img src= "/uploads/moviePosters/{{$movie['movie_id']}}.png">
		<br>
		Title: {{$movie['title']}}
		<br>
		Tagline: {{$movie['tagline']}}
		<br>
		Summary: {{$movie['overview']}}
		<br>
		Released: {{$movie['release_date']}}
		<br>
		Runtime: {{$movie['runtime']}} minutes
		<br>
		Average TMDB score: {{$movie['vote_average']}}
		<br>

		@if(auth()->id() == $user->id)

			<form method="POST" action="{{ route('deleteMovie', ['slug' => $movie->slug]) }}" id="movie_delete">
				{{ csrf_field() }}

				<input type="hidden" id = "movie_id" name="movie_id" value="{{$movie->id}}">

				<button type = "submit" class = "btn btn-danger btn-xs">Delete Movie</button>
			</form>		

		@endif

	@endforeach

@endsection