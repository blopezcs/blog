@extends('base')

@section('content')

@if(isset($error))
	<div class="alert alert-danger">{{$error}}</div>
@endif

<h1>Find your Movie by ID</h1>

<h4>The following search uses the TMDB API to search "The Movie Database" for movies. Enter the TMDB ID of a movie below to find it and be able to add it to your profile.</h4>
<h5>(to find the TMDB ID for a movie, look up a movie on their website and look in the URL of the page for the movie. The number in the URL is the ID.)</h5>

<form method="POST" action="/movieSearch/results" id="movie_id_match">
	{{ csrf_field() }}
	<div class = "form-group">
		<label for="movie_id">Movie ID</label>
		<input type="number" id = "movie_id" name="movie_id" required>
	</div>
	<button type = "submit" class = "btn btn-primary">Search</button>
</form>

@stop  