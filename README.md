# This is Brian Lopez's sample blog site

## About me:
I am a senior studying Computer Science at Duke University and built this website in order to learn more about web development.

## About the site:
* This website was built using Laravel 5.4
* The site supports basic features of a blog such as the ability to write posts, comment on posts, and view user profiles and the post history of users
* When writings posts and comments users have access to the TinyMCE WYSIWYG HTML editor
* Users can search the blog by searching user profiles, specific posts, and specific tags
* Users can also add their favorite movies to their profiles by searching for the movie using the "TMDB Movie Search" tab and adding it to their profile
* The movie search function uses the TMDb api

## To be implemented:
* The ability to search movies by title
* General improvements to the search function